git clone https://Lazerov@bitbucket.org/Lazerov/laravel-todolist.git

## Modifier l'accès à la base de donnée
app/config/local/database.php

```
'mysql' => array(
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'CHAMP A MODIFIER',
			'username'  => 'CHAMP A MODIFIER',
			'password'  => 'CHAMP A MODIFIER',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
		),
```
## Installer les dépendances
```
composer install
```

## Modifier l'environnement avec le nom de votre machine
```
$env = $app->detectEnvironment(array(

	'local' => array('NOM DU SERVEUR'),

));
```

## Effectuer la migration de la BDD
```
php artisan migrate
```

## Ajouter du contenue dans la BDD
```
php artisan db:seed
```