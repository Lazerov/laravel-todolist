<?php

////////// ACCUEIL
Route::get('/', 'HomeController@getIndex');


////////// Login
/* GET */
Route::get('login', 'HomeController@getLogin');

/* POST */
Route::post('login', 'HomeController@postLogin');


////////// Register
/* GET */
Route::get('register', 'HomeController@getRegister');

/* POST */
Route::post('register', 'HomeController@postRegister');


////////// TasksList
/* GET */
Route::get('list', 'HomeController@getList');

/* GET */
Route::get('add', 'HomeController@getAddTask');

/* POST */
Route::post('add', 'HomeController@postAddTask');

/* GET */
Route::get('update/{id}', 'HomeController@getUpdate');

/* POST */
Route::post('update/{id}', 'HomeController@postUpdate');

/* GET */
Route::get('remove/{id}', 'HomeController@getRemove');

/* POST */
Route::post('remove/{id}', 'HomeController@postRemove');


////////// Logout
/* GET */
Route::get('logout', 'HomeController@logout');

////////// 404
App::missing(function($exception){
    return Response::make('Page Not Found', 404);
});