<?php
class TasksTableSeeder extends Seeder {
    public function run() {
        DB::table('tasks')->insert(
            array(
                array(
                    'name' => 'boire',
                    'id_user' => '1'
                ),

                array(
                    'name' => 'manger',
                    'id_user' => '1'
                ),
 
                array(
                    'name' => 'Ne pas travailler',
                    'id_user' => '2'
                ),

                array(
                    'name' => 'travailler',
                    'id_user' => '2'
                ),
            )
        );
    }
}
