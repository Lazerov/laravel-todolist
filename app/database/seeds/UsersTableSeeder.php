<?php
class UsersTableSeeder extends Seeder {
    public function run() {
        DB::table('users')->insert(
            array(
                array(
                    'username' => 'Bob',
                    'password' => Hash::make('Passw0rd')
                ),
 
                array(
                    'username' => 'Georges',
                    'password' => Hash::make('Passw0rd')
                ),
            )
        );
    }
}
