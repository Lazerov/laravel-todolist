@include('layout.header')
@include('layout.navigation')
<h2>Add Task</h2>

{{ Form::open(array('url' => 'add', 'class' => 'form-horizontal')) }}
@if ($errors->any())
    <div class="row">
        <div class="alert alert-danger col-sm-offset-3 col-sm-6">
            <a href="#" title="errors" class="close" data-dismiss="alert">&times;</a>
            {{ implode('', $errors->all('<li style="text-align: center;">:message</li>')) }}
        </div>
    </div>
@endif

<div class="form-group">
    <label class="control-label col-sm-3" for="name">Task Name :</label>
    <div class="col-sm-6" ">
        <input type="text" class="form-control" id="name" name="name">
    </div>
</div>

<div class="col-sm-12" style="text-align: center;">
    <button type="submit" class="btn btn-success">Add</button>
</div>

{{ Form::close() }}

@include('layout.footer')