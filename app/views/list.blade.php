@include('layout.header')
@include('layout.navigation')
<h2>Tasks List</h2>

@for ($i = 0; $i < count($taskList); $i++)
	<div class="row list-task">
		<div class="col-sm-offset-3 col-sm-6">
			<div class="col-sm-2 list-task-id">
				<?= $taskList[$i]->id ?>
			</div>
			<div class="col-sm-6 list-task-name">
				<?= $taskList[$i]->name ?>
			</div>
			<div class="col-sm-2 list-task-update">
				<a href="update/<?= $taskList[$i]->id ?>" title="Update">
					<span class="glyphicon glyphicon-retweet"></span>
				</a>
			</div>
			<div class="col-sm-2 list-task-remove">
				<a href="remove/<?= $taskList[$i]->id ?>" title="Remove">
					<span class="glyphicon glyphicon-remove"></span>
				</a>
			</div>
		</div>
	</div>
@endfor
@include('layout.footer')