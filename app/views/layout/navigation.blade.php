<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="/">TodoList</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
      @if(Auth::user())
        <li><a href="/index.php/list">List</a></li>
        <li><a href="/index.php/add">Add Task</a></li>
      @else
        <li><a href="/"><span class="glyphicon glyphicon-home"></span> Home</a></li>
      @endif
      </ul>
      <ul class="nav navbar-nav navbar-right">
      @if(Auth::user())
        @if(isset($user->username))
          <li><a href="#"> Connecté en tant que : {{ $user->username }}</a></li>
        @endif
        <li><a href="/index.php/logout"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
      @else
        <li><a href="/index.php/login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
        <li><a href="/index.php/register"><span class="glyphicon glyphicon-user"></span> Register</a></li>
      @endif
      </ul>
    </div>
  </div>
</nav>