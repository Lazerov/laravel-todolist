@include('layout.header')
@include('layout.navigation')
<h2>Update Task : <?= $task[0]->id ?></h2>
<?php $idtask = $task[0]->id ?>
{{ Form::open(array('url' => "update/$idtask", 'class' => 'form-horizontal')) }}
@if ($errors->any())
    <div class="row">
        <div class="alert alert-danger col-sm-offset-3 col-sm-6">
            <a href="#" title="errors" class="close" data-dismiss="alert">&times;</a>
            {{ implode('', $errors->all('<li style="text-align: center;">:message</li>')) }}
        </div>
    </div>
@endif

<div class="form-group">
    <label class="control-label col-sm-3" for="name">Task Name :</label>
    <div class="col-sm-6" ">
        <input type="text" class="form-control" id="name" name="name" value="<?= $task[0]->name ?>">
    </div>
</div>

<div class="col-sm-12" style="text-align: center;">
    <button type="submit" class="btn btn-success">Update</button>
    <a href="/index.php/list"><div class="btn btn-danger">Cancel</div></a>
</div>

{{ Form::close() }}

@include('layout.footer')