@include('layout.header')
@include('layout.navigation')
<h2>Registration</h2>
{{ Form::open(array('url' => 'register', 'class' => 'form-horizontal')) }}
@if ($errors->any())
    <div class="row">
        <div class="alert alert-danger col-sm-offset-3 col-sm-6">
            <a href="#" title="errors" class="close" data-dismiss="alert">&times;</a>
            {{ implode('', $errors->all('<li style="text-align: center;">:message</li>')) }}
        </div>
    </div>
@endif

<div class="form-group">
    <label class="control-label col-sm-3" for="username">Username :</label>
    <div class="col-sm-6">
        <input type="text" class="form-control" id="username" name="username">
    </div>
</div>

<div class="form-group">
    <label class="control-label col-sm-3" for="password">Password :</label>
    <div class="col-sm-6">
        <input type="password" class="form-control" id="password" name="password">
    </div>
</div>
<div class="form-group">
    <label class="control-label col-sm-3" for="confirm">Confirm Password :</label>
    <div class="col-sm-6">
        <input type="password" class="form-control" id="confirm" name="confirm">
    </div>
</div>

<div class="col-sm-12" style="text-align: center;">
    <button type="submit" class="btn btn-success">Register</button>
</div>
{{ Form::close() }}

@include('layout.footer')