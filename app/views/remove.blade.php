@include('layout.header')
@include('layout.navigation')
<h2>Remove Task : <?= $task[0]->id ?></h2>
<?php $idtask = $task[0]->id ?>
{{ Form::open(array('url' => "remove/$idtask", 'class' => 'form-horizontal')) }}

<div class="col-sm-12" style="text-align: center;">
    <button type="submit" class="btn btn-danger">Remove</button>
    <a href="/index.php/list"><button role="button" class="btn btn-success">Cancel</button></a>
</div>

{{ Form::close() }}
@include('layout.footer')