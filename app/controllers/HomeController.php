<?php

class HomeController extends BaseController {

/////// Index
public function getIndex()
    {
        $data = [
                'title' => 'Home'
            ];
        if (Auth::check()) {
            $userId = Auth::id();
            $user = User::find($userId);
            return View::make('index', $data)->with('user', $user);
        } else {
            return View::make('index', $data);
        }
    }


/////// Login
    public function getLogin()
    {
        if (Auth::check()) {
            return Redirect::to('list');
        } else {
            $data = [
                'title' => 'Login'
            ];
            return View::make('login', $data);
        }
    }

    public function postLogin()
    {
        $input = Input::all();
        $rules = [
            'username' => 'required',
            'password' => 'required'
        ];
        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Redirect::to('login')->withErrors($validator);
        } else {
            $credentials = [
                'username' => $input['username'],
                'password' => $input['password']
            ];

            if (Auth::attempt($credentials)) {
                return Redirect::to('list');
            } else {
                return Redirect::to('login');
            }
        }
    }


/////// Register
    public function getRegister()
    {
        if (Auth::check()) {
            return Redirect::to('list');
        } else {
            $data = [
                'title' => 'Registration'
            ];
            return View::make('register', $data);
        }
    }

    public function postRegister()
    {
        $input = Input::all();
        $rules = [
            'username' => 'required|min:3|max:30|alpha_num|unique:users',
            'password' => 'required|min:4|max:12|alpha_num',
            'confirm' => 'required|same:password',
        ];
        $messages = [
            'required' => ':attribute est requis.',
        ];
        $validator = Validator::make($input, $rules, $messages);
        if ($validator->fails()) {
            return Redirect::to('register')->withInput()->withErrors($validator);
        } else {
            $password = $input['password'];
            $password = Hash::make($password);
            $user = New User();
            $user->username = $input['username'];
            $user->password = $password;
            $user->save();
            return Redirect::to('login');
        }
    }


/////// TaskList
    public function getList()
    {
        if (Auth::check()) {
            $data = [
                'title' => 'Tasks List'
            ];
            $id = Auth::id();
            $user = User::find($id);
            $taskList = DB::table('tasks')->where('id_user', $id)->get();
            return View::make('list', $data)->with('taskList', $taskList)->with('user', $user);
        } else {
            return Redirect::to('login');
        }
    }

    public function getAddTask()
    {
        if (Auth::check()) {
            $data = [
                'title' => 'Add Task'
            ];
            $id = Auth::id();
            $user = User::find($id);
            return View::make('add', $data)->with('user', $user);
        } else {
            return Redirect::to('login');
        }

    }

    public function postAddTask()
    {
        $input = Input::all();
        $id = Auth::id();
        $rules = [
                'name' => 'required|min:3|max:100|string'
            ];
        $messages = [
            'required' => ':attribute est requis.',
        ];
        $validator = Validator::make($input, $rules, $messages);
        if ($validator->fails()) {
            return Redirect::to('add')->withInput()->withErrors($validator);
        } else {
            DB::table('tasks')->insert(
                [
                    'name' => $input['name'],
                    'id_user' => $id
                ]
            );
            return Redirect::to('list');
        }
    }

    public function getUpdate($id)
    {
        if (Auth::check()) {
            $data = [
                'title' => 'Update Task'
            ];
            $id = Auth::id();
            $user = User::find($id);
            $task = DB::table('tasks')->where('id', $id)->get();
            return View::make('update', $data)->with('task', $task)->with('user', $user);
        } else {
            return Redirect::to('login');
        }
    }

    public function postUpdate($id)
    {
        $id_user = Auth::id();
        $input = Input::all();
        $rules = [
                'name' => 'required|min:3|max:100|string'
            ];
        $messages = [
            'required' => ':attribute est requis.',
        ];
        $validator = Validator::make($input, $rules, $messages);

        if ($validator->fails()) {
            return Redirect::to('add')->withInput()->withErrors($validator);
        } else {
            DB::table('tasks')->where('id', $id)->update(
                [
                    'name' => $input['name'],
                    'id_user' => $id_user
                ]
            );
            return Redirect::to('list');
        }
    }

    public function getRemove($id)
    {
        if (Auth::check()) {
            $data = [
                'title' => 'Remove Task'
            ];
            $id = Auth::id();
            $user = User::find($id);
            $task = DB::table('tasks')->where('id', $id)->get();
            return View::make('remove', $data)->with('task', $task)->with('user', $user);
        } else {
            return Redirect::to('login');
        }
    }

    public function postRemove($id)
    {
        DB::table('tasks')->where('id', '=', $id)->delete();
        return Redirect::to('list');
    }


/////// Logout
    public function logout()
    {
        Auth::logout();
        return Redirect::to('/');
    }

}
